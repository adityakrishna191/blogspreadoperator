# Spread Operator

Spread operator (`...`) is used to **expand** individual elements from an **iterable** (strings and arrays) in places where they need to be used. It can also be used with **object expression** to expand its key-value pair. A simple way to understand it is that when we simply have an array, its elements are bound and contained within `[,]` and when we use `(...)` the elements come out of that bound and can be seen as individual values. Let's go through some code examples to have a better understanding.
```js
let arr = [1,2,3];

console.log(arr); //-> [1, 2, 3]
console.log(...arr); //-> 1 2 3
```

We can use spread to add elements from an existing array to a new array, it gives us the power to do more with less code
```js
let arr1 = [1,2,3]; // We want to get elements of arr1 into a new arr2

// before spread
let arr2 = [arr1[0], arr1[1], arr1[2], 4];
console.log(arr2); //-> [ 1, 2, 3, 4 ]

// after spread
let arr2 = [...arr1, 4];
console.log(arr2); //-> [ 1, 2, 3, 4 ]
```
We can merge multiple arrays elements into one new array using spread
```js
let arr1 = [2, 3, 4];
let arr2 = [6, 7, 8];

let arr3NoSpread = [1,arr1, 5, arr2, 9]; //-> [ 1, [ 2, 3, 4 ], 5, [ 6, 7, 8 ], 9 ]
let arr3WithSpread = [1, ...arr1, 5, ...arr2, 9]; //-> [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]

console.log(arr3NoSpread);
console.log(arr3WithSpread);
```

We can use spread to include elements of an array as arguments in a function call
```js
// Here we are declaring a function sumOf which takes two arguments and returns sum of them
const sumOf = (param1, param2) => {
    return param1 + param2;
}
// Without spread
console.log(sumOf([1, 2])); //-> 1,2undefined
// With spread
console.log(sumOf(...[1, 2])); //-> 3
```
Let's examine above example and try to break what's happening:
- When we don't use spread, the function takes `[1, 2]` as the first argument and since there is no other argument, it assigns `undefined` as the second argument and returns the sum as concatenation of `1,2` and `undefined` (because of **type coercion**)
- When we use (`...`), the elements of array are assigned to the `param1 = 1` and `param2 = 2` and sum as `3 = 1 + 2` gets returned.

We can use spread to make shallow copy of objects
```js
//Before spread
let arr1 = [1, 2, 3];
let arr2 = arr1;
console.log(arr1===arr2); //-> true
//After spread
let arr2 = [...arr1];
console.log(arr1===arr2); //-> false
```
Before spread if we would copy `arr1` to `arr2`, the **reference** of `arr1` gets copied inside `arr2`, so if any changes were to be made to `arr2` it would also impact `arr1`, which is _not good practice_. But after using spread, we first expand the elements of `arr1` and then assign it to `arr2`, not the reference. So any changes made to `arr2` will not impact `arr1`.

---
### References
- [MDN document on Spread syntax](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_syntax)
- [Javascript Info](https://javascript.info/rest-parameters-spread)